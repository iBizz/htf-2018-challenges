# Hack the future 2018

## Challenge 1
Before you can start unraveling the artifacts secrets you need to secure the area around the artifact from curious civilians and from competitors trying to get a glimpse of the artifact.
There are existing security cameras around the area but no user friendly way to monitor the cameras.
Build a security dashboard to monitor the security feed of the camera(s).

The dashboard need following functionality:
* Small preview of all security feeds (page 1)
* Large single security feed (page 2)
  * Save current frame
  * Overview of saved frames
  * Basic information of people in the saved frame (age,gender, facial features)

Endpoints for challenge:
* /stream --> returns a list of stream ids
* /stream/{id} --> replace {id} with an id
  * returns 404 for invalid id
  * returns 200 and multipart image form for valid id

To get the basic information of people in the frame use the Microsoft cognitive services:
  * Get the 7 day trial for [Microsoft cognitive services](https://azure.microsoft.com/en-us/services/cognitive-services/face/)
  * [Face api reference](https://westus.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/563879b61984550f30395236)

## Challenge 2
Great the area is secured! Now we can start by unveiling the secrets of the artifact in peace.

The symbols look a lot like letters from our alphabet, yet they don't have seem to have the same meaning, our scientist can't figure it out.
Luckily we can use AI to help us figure out what the symbols mean.

We took pictures of the symbols on the artifact and bundled them in a zip file for your convenience.

Tasks:<br>
Build an application that lets you upload one of the pictures to the AI REST api and keep track of the responses.

## Challenge 3
Now that we deciphered the images gathered from the artifact, it is time to crack the code.
Tasks:<br>
Write a program that checks if the password is correct using the given API. You can do this any way you want but remember the API will give you random tips when you guess wrong.

Endpoind:
* /password/{check}
  * returns 200 for correct password
  * returns 418 for incorrect password

## Challenge 4
Everything is looking great so far! But it is time to integrate everything together into a nice UI that is easy to use.
Build a user friendly UI that integrates everything from the previous challenges.

## Challenge 5
???
